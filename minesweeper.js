const readline = require('readline')
readline.emitKeypressEvents(process.stdin)
const readlineInstance = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: true
})

const bombSpace = String.fromCharCode(9762)
const cursorSpace = '_'
const uncoveredSpace = ' '
const flagSpace = String.fromCharCode(9635)
const coveredSpace = String.fromCharCode(9634)
const positions = [
  { y: -1, x: 0 },
  { y: -1, x: 1 },
  { y: 0, x: 1 },
  { y: 1, x: 1 },
  { y: 1, x: 0 },
  { y: 1, x: -1 },
  { y: 0, x: -1 },
  { y: -1, x: -1 }
]

const clamp = (number, min, max) => number <= min ? min : number >= max ? max : number
const writeLine = line => line.reduce((acc, val) => `${acc} ${val}`, '')
const writeGrid = columns => columns.reduce((acc, val) => `${acc}\n${writeLine(val)}`, '')
const question = question => new Promise(resolve => readlineInstance.question(question, answer => resolve(answer)))
const fill = (length, value) => Array.from({ length }).map(_ => cloneArray(value))
const cloneArray = value => Array.isArray(value) ? [...value] : value
const clone = value => JSON.parse(JSON.stringify(value))
const inBounds = (size, y, x) => y >= 0 && y < size && x >= 0 && x < size

const start = async () => {
  let cursor = { y: 0, x: 0 }
  let grid = []
  let playerGrid = []
  let size = 10
  let difficulty = 0.15
  let answer

  answer = await question('What is your grid size? (min 5, max 40) ')
  if (isNaN(parseInt(answer, 10))) throw new Error('Bad size input.')
  size = clamp(parseInt(answer), 5, 40)
  console.log(`Size set to ${size}`)

  answer = await question('What is your difficulty? (min 0.1, max 0.3) ')
  if (isNaN(parseFloat(answer, 10))) throw new Error('Bad difficulty input.')
  difficulty = clamp(parseFloat(answer), 0.1, 0.3)
  console.log(`Difficulty set to ${difficulty}`)

  grid = fill(size, fill(size, 0))
  playerGrid = fill(size, fill(size, coveredSpace))
  const totalBombs = Math.round(size * size * difficulty)
  let bombs = 0
  let flagCount = totalBombs
  let bombsFound = 0

  while (bombs < totalBombs) {
    const y = Math.floor(Math.random() * size)
    const x = Math.floor(Math.random() * size)

    if (grid[y][x] !== 9) {
      grid[y][x] = 9
      bombs++
    }
  }

  for (let y = 0; y < size; y++) {
    for (let x = 0; x < size; x++) {
      if (grid[y][x] !== 9) {
        for (let i in positions) {
          const yBomb = positions[i].y + y
          const xBomb = positions[i].x + x
          if (inBounds(size, yBomb, xBomb) && grid[yBomb][xBomb] === 9) grid[y][x]++
        }
      }
    }
  }
  
  showGrid(playerGrid, cursor)

  process.stdin.on('keypress', (_, key) => {
    if (key.ctrl && key.name === 'c') process.exit()

    switch (key.name) {
      case 'up':
        cursor = inBounds(size, cursor.y - 1, cursor.x) ? { ...cursor, y: cursor.y - 1 } : cursor
        break
      case 'right':
        cursor = inBounds(size, cursor.y, cursor.x + 1) ? { ...cursor, x: cursor.x + 1 } : cursor
        break
      case 'down':
        cursor = inBounds(size, cursor.y + 1, cursor.x) ? { ...cursor, y: cursor.y + 1 } : cursor
        break
      case 'left':
        cursor = inBounds(size, cursor.y, cursor.x - 1) ? { ...cursor, x: cursor.x - 1 } : cursor
        break
      case 'space':
        playerGrid = uncoverSpace(playerGrid, grid, size, cursor.y, cursor.x)
        break
      case 'x':
        if (flagCount && playerGrid[cursor.y][cursor.x] === coveredSpace) {
          playerGrid[cursor.y][cursor.x] = flagSpace
          flagCount--
          if (grid[cursor.y][cursor.x] === 9) bombsFound++
        } else if (playerGrid[cursor.y][cursor.x] === flagSpace) {
          playerGrid[cursor.y][cursor.x] = coveredSpace
          flagCount++
          if (grid[cursor.y][cursor.x] === 9) bombsFound--
        }

        if (bombsFound === bombs) {
          playerGrid = revealAll(playerGrid, grid, size)
          showGrid(playerGrid)
          console.log('You won!')
          process.exit()
        }

        break
    }

    showGrid(playerGrid, cursor)
  })
}

const showGrid = (playerGrid, cursor = null) => {
  console.clear()
  const viewGrid = clone(playerGrid)

  if (cursor) {
    viewGrid[cursor.y][cursor.x] = cursorSpace
  }

  console.log(writeGrid(viewGrid))
}

const revealAll = (playerGrid, grid, size, win = true) => {
  for (let y = 0; y < size; y++) {
    for (let x = 0; x < size; x++) {
      if (grid[y][x] === 9) playerGrid[y][x] = win ? flagSpace : bombSpace
      else if (grid[y][x] === 0) playerGrid[y][x] = uncoveredSpace
      else playerGrid[y][x] = grid[y][x]
    }
  }

  return playerGrid
}

const uncoverSpace = (playerGrid, grid, size, y, x) => {
  if (playerGrid[y][x] === coveredSpace) {
    if (grid[y][x] === 9) {
      playerGrid = revealAll(playerGrid, grid, size, false)
      showGrid(playerGrid)
      console.log('You lost!')
      process.exit()
    } else if (grid[y][x] === 0) {
      playerGrid[y][x] = uncoveredSpace

      for (let i in positions) {
        const yUncover = positions[i].y + y
        const xUncover = positions[i].x + x

        if (inBounds(size, yUncover, xUncover) && grid[yUncover][xUncover] !== 9) {
          uncoverSpace(playerGrid, grid, size, yUncover, xUncover)
        }
      }
    } else {
      playerGrid[y][x] = grid[y][x]
    }
  }

  return playerGrid
}

start()
